# Test assigment for Coins.ph

## How to run fast
`docker-compose up`

## How to set up local environment
`pip install -r requirements.txt`

`python manage.py migrate`

`python manage.py createsuperuser`

`python manage.py runserver`


## Deployment guides:
1. Local deployment: `docs/deploy/Local.md`
2. Cluster guide: `docs/deploy/Cluster.md`

## Kubernetes Dashboard
`77.244.214.196:30170`

## Postman collection
`postman/coins.postman_collection.json`
