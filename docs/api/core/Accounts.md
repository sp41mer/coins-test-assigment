# Accounts
Supports viewing accounts.

## Get a user's profile information

**Request**:

`GET` `/api/core/accounts/:id/`

*Note:*

- **Not protected by authorization**

**Response**:

```json
Content-Type application/json
200 OK

{
    "id": 1,
    "account_id": "Bob",
    "balance": "50.00",
    "currency": "USD"
}
```

## Get all users in system

**Request**:

`GET` `/api/core/accounts/`

Parameters:

*Note:*

- **Not protected by authorization**

**Response**:

```json
Content-Type application/json
200 OK

[
    {
        "id": 1,
        "account_id": "Bob",
        "balance": "50.00",
        "currency": "USD"
    },
    {
        "id": 2,
        "account_id": "Alice",
        "balance": "130.00",
        "currency": "USD"
    },
    {
        "id": 3,
        "account_id": "Eva",
        "balance": "120.00",
        "currency": "EURO"
    }
]
```
