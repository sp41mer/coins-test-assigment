# Users
Supports creating and viewing payments.

## Create new payment

**Request**:

`POST` `/api/core/payments/`

Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
from_account   | string | Yes      | Outgoing account.
to_account   | string | Yes      | Incoming account.
amount | string | Yes       | Amount of currency.

*Note:*

- **Not protected by authorization**

**Response**:

```json
Content-Type application/json
201 Created

{
    "id": 6,
    "from_account": "Bob",
    "to_account": "Alice",
    "amount": "10.00"
}
```

## Get payment information

**Request**:

`GET` `/api/core/payments/:id/`

Parameters:

*Note:*

- **Not protected by authorization**

**Response**:

```json
Content-Type application/json
200 OK

{
    "id": 1,
    "from_account": "Bob",
    "to_account": "Alice",
    "amount": "10.00"
}
```

## Get information about all payments

**Request**:

`GET` `/api/core/payments/`

Parameters:

*Note:*

- **Not protected by authorization**

**Response**:

```json
Content-Type application/json
200 OK

[
    {
        "id": 1,
        "from_account": "Bob",
        "to_account": "Alice",
        "amount": "10.00"
    },
    {
        "id": 2,
        "from_account": "Bob",
        "to_account": "Alice",
        "amount": "10.00"
    },
]
```
