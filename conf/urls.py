from django.contrib import admin
from django.urls import path, include

from core.router import router as core_router

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/core/', include(core_router.urls))
]
