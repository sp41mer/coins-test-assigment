from rest_framework import serializers

from core.models import Payment, Account


class PaymentSerializer(serializers.ModelSerializer):
    from_account = serializers.SlugRelatedField(queryset=Account.objects.all(), slug_field='account_id')
    to_account = serializers.SlugRelatedField(queryset=Account.objects.all(), slug_field='account_id')
    amount = serializers.DecimalField(decimal_places=2, max_digits=12)

    class Meta:
        model = Payment
        fields = ('id', 'from_account', 'to_account', 'amount')

    def validate(self, data: dict) -> dict:
        """
        Validation for:
        1) Availability of payment
        2) Same currency
        :param data: prevalidated from serializer
        :return: validated data
        """
        if data['amount'] > data['from_account'].balance:
            raise serializers.ValidationError('Insufficient funds')
        if data['from_account'].currency != data['to_account'].currency:
            raise serializers.ValidationError('Only same currency allowed')

        return data
