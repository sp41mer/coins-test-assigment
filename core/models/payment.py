from decimal import Decimal

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models, transaction

from core.models.base import FullCleanModel
from .account import Account


class PaymentManager(models.Manager):

    def create(self, **kwargs):
        """
        Overriding of objets.create() method, to make operation with balances in same transaction with payment creation
        :param kwargs: standard kwargs for objets.create()
        :return: instance of Payment model
        """
        with transaction.atomic():
            payment: Payment = self.model(**kwargs)
            payment.from_account.decrease_balance(payment.amount)
            payment.to_account.increase_balance(payment.amount)
            return super().create(**kwargs)


class Payment(FullCleanModel):

    from_account = models.ForeignKey(Account, on_delete=models.PROTECT, editable=False, related_name='outcome_payments')
    to_account = models.ForeignKey(Account, on_delete=models.PROTECT, editable=False, related_name='income_payments')
    amount = models.DecimalField(decimal_places=2, max_digits=12,
                                 validators=[MinValueValidator(Decimal('0.01'))], editable=False)

    objects = PaymentManager()

    def clean(self) -> None:
        """
        Validation for currencies
        :return:
        """
        if self.from_account.currency != self.to_account.currency:
            raise ValidationError('Only same currency allowed')
