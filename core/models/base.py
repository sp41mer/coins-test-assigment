from django.db import models


class FullCleanModel(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """
        Overriding to use .full_clean method
        :param args: standard args for Model.save()
        :param kwargs: standard kwargs for Model.save()
        :return: this function doesn't return anything
        """
        self.full_clean()
        return super().save(*args, **kwargs)
