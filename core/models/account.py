from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models


class Account(models.Model):

    USD, EURO = 'USD', 'EURO'

    CURRENCY_CHOICES = (
        (USD, 'American Dollars'),
        (EURO, 'Euros')
    )

    account_id = models.CharField(unique=True, max_length=36)
    balance = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))])
    currency = models.CharField(choices=CURRENCY_CHOICES, max_length=36)

    def decrease_balance(self, amount: Decimal) -> None:
        """
        Function to decrease balance of user.
        :param amount: amount to decrease
        :return: this function doesn't return anything
        """
        self.balance -= amount
        self.save()

    def increase_balance(self, amount: Decimal) -> None:
        """
        Function to increase balance of user.
        :param amount: amount to increase
        :return: this function doesn't return anything
        """
        self.balance += amount
        self.save()
