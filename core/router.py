from rest_framework.routers import DefaultRouter
from core import views

router = DefaultRouter(trailing_slash=True)

router.register('payments', views.Payments, 'payments')
router.register('accounts', views.Accounts, 'accounts')
