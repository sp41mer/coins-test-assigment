from django.contrib import admin
from core import models

admin.site.register(models.Payment)
admin.site.register(models.Account)
