from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import ReadOnlyModelViewSet

from core.models import Payment
from core.serializers.payments import PaymentSerializer


class Payments(ReadOnlyModelViewSet, CreateModelMixin):
    queryset = Payment.objects.all().select_related('from_account', 'to_account')
    serializer_class = PaymentSerializer
