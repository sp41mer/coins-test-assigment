from rest_framework.viewsets import ReadOnlyModelViewSet

from core.models import Account
from core.serializers.accounts import AccountSerializer


class Accounts(ReadOnlyModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
