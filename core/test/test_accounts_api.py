import json

from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response

from . import BaseApiTest


class AccountApiTest(BaseApiTest):

    def test_list(self):
        api_json = json.loads(json.dumps([
            {
                "account_id": self.account_one.account_id,
                "balance": str(self.initial_balance),
                "currency": self.account_one.currency
            },
            {
                "account_id": self.account_two.account_id,
                "balance": str(self.initial_balance),
                "currency": self.account_two.currency
            },
            {
                "account_id": self.account_wrong_currency.account_id,
                "balance": str(self.initial_balance),
                "currency": self.account_wrong_currency.currency
            },
        ]))
        response: Response = self.client.get(reverse('accounts-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), api_json)
