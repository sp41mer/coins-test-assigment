import json
from decimal import Decimal

from django.urls import reverse
from rest_framework import status

from . import BaseApiTest


class PaymentApiTest(BaseApiTest):

    def test_list(self):
        api_json = json.loads(json.dumps([
            {
                "from_account": self.account_one.account_id,
                "to_account": self.account_two.account_id,
                "amount": str(self.payment.amount)
            }

        ]))
        response = self.client.get(reverse('payments-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), api_json)

    def test_create_201_ok(self):
        request = self._create_payment_request()
        response = self.client.post(reverse('payments-list'), data=request, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_check_balances(self):
        amount = Decimal(20.0)
        account_one_before_balance = self._get_account_balance(self.account_one)
        account_two_before_balance = self._get_account_balance(self.account_two)
        request = self._create_payment_request(amount=amount)
        response = self.client.post(reverse('payments-list'), data=request, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self._get_account_balance(self.account_one), account_one_before_balance - amount)
        self.assertEqual(self._get_account_balance(self.account_two), account_two_before_balance + amount)

    def test_check_invalid_currency(self):
        request = self._create_payment_request(account_two=self.account_wrong_currency)
        response = self.client.post(reverse('payments-list'), data=request, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_check_large_amount(self):
        amount = self.account_one.balance + Decimal(20.0)
        request = self._create_payment_request(amount=amount)
        response = self.client.post(reverse('payments-list'), data=request, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
