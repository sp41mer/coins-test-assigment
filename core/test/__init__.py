from decimal import Decimal

from rest_framework.test import APIClient, APITestCase
from django_dynamic_fixture import G

from core.models import Account, Payment


class BaseApiTest(APITestCase):

    initial_balance = Decimal('100.00')

    @staticmethod
    def _get_account_balance(account: Account) -> Decimal:
        return Account.objects.get(account_id=account.account_id).balance

    def _create_payment_request(self, **kwargs) -> dict:
        return {
            "from_account": kwargs.get('account_one', self.account_one).account_id,
            "to_account": kwargs.get('account_two', self.account_two).account_id,
            "amount": kwargs.get('amount', Decimal(10.0))
        }

    def setUp(self):
        self.client: APIClient = APIClient()
        self.account_one: Account = G(Account, account_id='Bob', balance=self.initial_balance, currency=Account.USD)
        self.account_two: Account = G(Account, account_id='Alice', balance=self.initial_balance, currency=Account.USD)
        self.account_wrong_currency: Account = G(Account, account_id='Eva',
                                                 balance=self.initial_balance, currency=Account.EURO)
        self.payment: Payment = G(Payment, from_account=self.account_one, to_account=self.account_two,
                                  amount=Decimal('10.00'))
